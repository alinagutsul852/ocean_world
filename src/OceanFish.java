public class OceanFish extends OceanСitizen {
    private int speed;
    private String type; //планктон, нектон(в толще воды), нектобентос(донные)
    private String skeletonType; //костные и хрящевые
    private int temperatureBody;

         public OceanFish(String name,
                     int lifetime,
                     String habitat,
                     String coloration,
                     int weight,
                     int speed,
                     String type,
                     String skeletonType,
                     int temperatureBody) {
        super(name, lifetime, habitat, coloration, weight);
        this.speed = speed;
        this.type = type;
        this.skeletonType = skeletonType;
        this.temperatureBody = temperatureBody;
    }

    public void stayingPowers(int temperatureWater){

        if(this.temperatureBody - temperatureWater == 1){
            System.out.println("All is well. The fish is healthy");
        } else if (this.temperatureBody - temperatureWater > 1){
            System.out.println("It looks like something is wrong with fish.");
        } else if (temperatureBody - temperatureWater < -10){
            System.out.println("Probably the fish is dead");
        } else {
            System.out.println("Maybe the fish is sick.");
        }
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSkeletonType() {
        return skeletonType;
    }

    public void setSkeletonType(String skeletonType) {
        this.skeletonType = skeletonType;
    }

    public int getTemperatureBody() {
        return temperatureBody;
    }

    public void setTemperatureBody(int temperatureBody) {
        this.temperatureBody = temperatureBody;
    }

   @Override
    public String toString() {
        return super.toString()
                .concat(String.valueOf(speed) + ", ")
                .concat(type + ", ")
                .concat(skeletonType + ", ")
                .concat(String.valueOf(temperatureBody))
                .concat("}");
    }
}
