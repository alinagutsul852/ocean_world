public class Main {
    public static void main(String[] args) {
        OceanFish shark = new OceanFish("Shark1", 35, "thickness", "white", 100, 70, "nekton", "cartilaginous", 20);
        shark.setSpeed(158);
        OceanСitizen carp = new OceanFish("carp1", 5, "surface", "gray", 6, 20, "nekton", "bony", 15);
        OceanСitizen anchovy = new OceanFish("anchovy1", 2, "surface", "white", 1, 2, "nekton", "withoutBone", 8);
        OceanСitizen salmon = new OceanFish("salmon1", 3, "thickness", "red", 10, 35, "nekton", "bony", 9);
        OceanСitizen bass = new OceanFish("bass1", 1, "surface", "black", 3, 26, "nekton", "bony", 15);
        OceanСitizen catfish = new OceanFish("catfish1", 5, "bottom", "black", 10, 18, "nektobenthos", "bony", 10);
        OceanСitizen kelp = new OceanPlant("kelp1", 4, "surface", "green", 3, "industrial", "year-round", "clean");
        OceanСitizen seaGrass = new OceanPlant("seaGrass", 2, "bottom", "reen", 4, "nonIndustrial", "year-round", "unclean");

        Ocean atlanticOcean = new Ocean("Atlantic", 127, 35);
        atlanticOcean.addCitizen(carp);
        atlanticOcean.addCitizen(anchovy);
        atlanticOcean.addCitizen(salmon);
        System.out.println(atlanticOcean.getCitizens());

        seaGrass.setLifetime(5);
        System.out.println(seaGrass.getLifetime());

        System.out.println(salmon.getColoration());

        salmon.catchingFish(9);

         OceanFish fish5 = new OceanFish("fish-saw",6, "bottom", "blue",8,36,"nekton","bony",18);
         fish5.stayingPowers(8);



    }
}






