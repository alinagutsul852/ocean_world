public class OceanСitizen {
    private String name;
    private int lifetime; // продолжительность жизни
    private String habitat; //среда обитания - на дне, на поверхности, в толщине
    private String coloration;
    private int weight; // вес - маленькая, средняя или большая

    public OceanСitizen(String name, int lifetime, String habitat, String coloration, int weight) {
        this.name = name;
        this.lifetime = lifetime;
        this.habitat = habitat;
        this.coloration = coloration;
        this.weight = weight;
    }
    public void catchingFish(int kilogram)
    {
        if (this.weight - kilogram <= 1) {
            System.out.println("You caught too small fish ");
        } else {
            this.weight -= kilogram;
            System.out.println("you caught a really big fish " + "this is " + this.name + " and weight is " + this.weight);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getColoration() {
        return coloration;
    }

    public void setColoration(String coloration) {
        this.coloration = coloration;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    @Override
    public String toString() {
        return new StringBuilder()
                .append("{")
                .append(name + ", ")
                .append(lifetime + ", ")
                .append(habitat + ", ")
                .append(coloration + ", ")
                .append(weight + ", ")
                .toString();

    }
}
