public class OceanPlant extends OceanСitizen {

    private String industrial; //используются человеком в питании или удобрениях,фармацевтике
    private String seasonality;
    private String cleansing;

    public OceanPlant (String name,
                       int lifetime,
                       String habitat,
                       String coloration,
                       int weight,
                       String industrial,
                       String seasonality,
                       String cleansing){
        super(name, lifetime, habitat, coloration, weight);
        this.industrial = industrial;
        this.seasonality = seasonality;
        this.cleansing = cleansing;
    }

    public String getIndustrial() {
        return industrial;
    }

    public void setIndustrial(String industrial) {
        this.industrial = industrial;
    }

    public String getSeasonality() {
        return seasonality;
    }

    public void setSeasonality(String seasonality) {
        this.seasonality = seasonality;
    }

    public String getCleansing() {
        return cleansing;
    }

    public void setCleansing(String cleansing) {
        this.cleansing = cleansing;
    }

   /* @Override
    public String toString() {
        return super.toString()
                .concat(industrial + ", ")
                .concat(seasonality + ", ")
                .concat(cleansing)
                .concat("}");
                }*/

}
