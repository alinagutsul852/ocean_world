import java.util.LinkedList;
import java.util.List;

public class Ocean {
    private String name;
    private  int depth; //глубина
    protected int salinity;  //соленость
//    private int toxins;  // токсичность
//    private int pressure;      //давление
//    private String color; //"lightblue";
//    private byte temperature;
//    private String locality;  //местность
//    private String flow; //"cold";
    private List<OceanСitizen> citizens = new LinkedList<>();

    public Ocean(String name, int depth, int salinity) {
        this.name = name;
        this.depth = depth;
        this.salinity = salinity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getSalinity() {
        return salinity;
    }

    public void setSalinity(int salinity) {
        this.salinity = salinity;
    }

    public List<OceanСitizen> getCitizens() {
        return citizens;
    }

    public void addCitizen(OceanСitizen citizen) {
        this.citizens.add(citizen);
    }
}

